//JOSE_LUIS MARQUES_FERNANDEZ 2015
//CSV PARSER
//OPTIMIZED FOR DEBIAN 8

#ifndef FHL_HPP_INCLUDED
#define FHL_HPP_INCLUDED

/*
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
//#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>

#include <algorithm>
*/

#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>


//Functional
std::string LF(std::string filename);

int SF(std::string filename, std::string data);//Returns 0 if save file is done correctly

int AF(std::string filename, std::string data);//Returns 0 if append to file is done correctly

std::vector<std::string> SBSV(std::string stdata);// Splits a string buffer in to a string vector using new lines as token

std::vector<std::vector<std::string>> TSSV (std::string stdata, std::string token);//Toketed string to string vector

std::string SVTS(std::vector<std::vector<std::string>> svdata, std::string token);//String vector to toketed string

std::vector<std::vector<double>> SVFV(std::vector<std::vector<std::string>> stdata);//String vector to float vector

std::vector<std::vector<std::string>> FVSV(std::vector<std::vector<double>> fvdata, int sigdigits=8);//Float vector to string vector

std::vector<std::vector<double>> FVDV(std::vector<std::vector<float>> vect);//Float vector to double vector

std::vector<std::vector<float>> DVFV(std::vector<std::vector<double>> vect);//Double vector to float vector


#endif
