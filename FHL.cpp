#include"FHL.hpp"

//Functional
std::string LF(std::string filename){//Load file. Returns file as string
	std::fstream FILEDATA;
	std::stringstream BUFFER;

	FILEDATA.open(filename, std::ios::in);
	BUFFER << FILEDATA.rdbuf();

	return BUFFER.str();
}

int SF(std::string filename, std::string data){//Returns 0 if save file is done correctly
	std::ofstream DATAFILE;

	DATAFILE.open(filename, std::ofstream::out | std::ofstream::trunc);

	if (DATAFILE.is_open()) {
		DATAFILE << data;
		DATAFILE.close();
		return 0;
	}
	else{
		std::cout<<"ERROR SAVING CSV"<<std::endl;
		return 1;
	}
}

int AF(std::string filename, std::string data){//Returns 0 if append to file is done correctly
	std::ofstream DATAFILE;

	DATAFILE.open(filename, std::ios_base::app);

	if (DATAFILE.is_open()) {
		DATAFILE << data;
		DATAFILE.close();
		return 0;
	}
	else{
		std::cout<<"ERROR APPENDING CSV"<<std::endl;
		return 1;
	}

}

std::vector<std::string> SBSV(std::string stdata){// Splits a string buffer in to a string vector using new lines as token
	std::string SVDATA=stdata+"\n";//Original string with added token at the end
	std::vector<std::string> LINES;

	std::string::size_type LPOS=0;//Line token positions
	std::string::size_type LLEN=0;//Line between tokens length
	std::string::size_type LTOKS=1;//Line token size
	for(int i=0;i<SVDATA.size();i+=1){//Line splitter
		if((SVDATA[i]=='\n')||(SVDATA[i]=='\r')||(SVDATA[i]=='\0')||(SVDATA[i]=='\f')){//New line split
			LLEN=i-LPOS;
			LINES.push_back(SVDATA.substr(LPOS,LLEN));
			LPOS=i+LTOKS;
			//std::cout<<"LINE::"<<LINE<<std::endl;
		}
	}

	return LINES;

}

std::vector<std::vector<std::string>> TSSV(std::string svdata, std::string token){//Toketed string to string vector
	std::string SVDATA=svdata+"\n";//Original string with added token at the end
	std::vector<std::vector<std::string>> STRVECT;

	std::string LINE;
	std::string::size_type LPOS=0;//Line token positions
	std::string::size_type LLEN=0;//Line between tokens length
	std::string::size_type LTOKS=1;//Line token size
	for(int i=0;i<SVDATA.size();i+=1){//Line splitter
		if((SVDATA[i]=='\n')||(SVDATA[i]=='\r')||(SVDATA[i]=='\0')||(SVDATA[i]=='\f')){//New line split
			LLEN=i-LPOS;
			LINE=SVDATA.substr(LPOS,LLEN);
			LINE=LINE+token;//Add additional token to the end
			LPOS=i+LTOKS;
			//std::cout<<"LINE::"<<LINE<<std::endl;

			std::string VALUE;
			std::vector<std::string> ILSV;//In line string vector
			std::string::size_type VPOS=0;//Value token positions
			std::string::size_type VLEN=0;//Value between tokens length
			std::string::size_type VTOKS=token.size();//Value token size
			for (int ii=0;ii<LINE.size();ii+=1){//Line iterator token search
				if(LINE.compare(ii,VTOKS,token) == 0){//Multi character token finder
					VLEN=ii-VPOS;
					VALUE=LINE.substr(VPOS,VLEN);
					ILSV.push_back(VALUE);
					VPOS=ii+VTOKS;
					ii+=(VTOKS-1);
					//std::cout<<"VALUE::"<<VALUE<<std::endl;
				}
			}
			STRVECT.push_back(ILSV);
		}
	}
	return STRVECT;
}

std::string SVTS(std::vector<std::vector<std::string>> svdata, std::string token){//String vector to toketed string
	std::string TOKSTR;//Tokketed string

	for(int i=0;i<svdata.size();i++){
		for(int ii=0;ii<svdata[i].size();ii++){
			TOKSTR=TOKSTR+svdata[i][ii];
			if(ii+1<svdata[i].size())TOKSTR=TOKSTR+token;//If column left add token
		}
		if(i+1<svdata.size())TOKSTR=TOKSTR+"\n";//If rows left add new line

	}

	return TOKSTR;
}

std::vector<std::vector<double>> SVFV(std::vector<std::vector<std::string>> stdata){//String vector to float vector
	std::vector<std::vector<double>> FLVECT;//Float vector

	for(int i=0;i<stdata.size();i++){
		std::vector<double> tFLVECT;
		for(int ii=0;ii<stdata[i].size();ii++){
			try {
				//float NUMBER = std::stof(stdata[i][ii]);
				double NUMBER = std::stod(stdata[i][ii]);
				//long double NUMBER = std::stold(stdata[i][ii]);
				tFLVECT.push_back(NUMBER);
			}
			catch(std::invalid_argument const& ex){//string has no number
				//std::cout << "std::invalid_argument::what(): " << ex.what() << '\n';
				//tFLVECT.push_back(0);
			}
		}
		FLVECT.push_back(tFLVECT);
	}
	return FLVECT;
}

std::vector<std::vector<std::string>> FVSV(std::vector<std::vector<double>> fvdata, int sigdigits){//Float vector to string vector
	std::vector<std::vector<std::string>> STVECT;//String vector

		for(int i=0;i<fvdata.size();i++){
		std::vector<std::string> tSTVECT;
		for(int ii=0;ii<fvdata[i].size();ii++){
			std::stringstream STRS;
			STRS << std::fixed << std::setprecision(sigdigits) << fvdata[i][ii];
			tSTVECT.push_back(STRS.str());
		}
		STVECT.push_back(tSTVECT);
	}
	return STVECT;
}

std::vector<std::vector<double>> FVDV(std::vector<std::vector<float>> vect){//Float vector to double vector
std::vector<std::vector<double>> VECT;

		for(int i=0;i<vect.size();i++){
			std::vector<double> tVECT;
			for(int ii=0;ii<vect[i].size();ii++){
				tVECT.push_back((double)vect[i][ii]);
			}
			VECT.push_back(tVECT);
		}
	return VECT;

}

std::vector<std::vector<float>> DVFV(std::vector<std::vector<double>> vect){//Double vector to float vector
std::vector<std::vector<float>> VECT;

		for(int i=0;i<vect.size();i++){
			std::vector<float> tVECT;
			for(int ii=0;ii<vect[i].size();ii++){
				tVECT.push_back((float)vect[i][ii]);
			}
		VECT.push_back(tVECT);
		}
	return VECT;
}
